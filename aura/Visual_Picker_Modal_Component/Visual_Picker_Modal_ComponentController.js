({
    doInit : function(component, event, helper) {
 
        var sObjName = component.get("v.sObjectName");
        if(sObjName){
            component.set("v.sSelectedOption",component.get("v.sFieldValue"));
            helper.getLabelForField(component, sObjName);
            helper.getOptionsForField(component, sObjName,component.get("v.sFieldName"), component.get("v.sFieldValue"));
        }
    },
    
    handleOptionSelected : function (component, event, helper){
        //Getting Option value from the Event where the Item is telling us what is the new Account selected
        var optionValue = event.getParam('optionValue');
        component.set("v.sSelectedOption",optionValue);
        helper.evaluateItemSelectedOrNot(component,optionValue);
    },

    handleSaveOption : function (component, event, helper){

        // Informing the base component to save the option
            var sendMsgEvent = $A.get("e.ltng:sendMessage");
            sendMsgEvent.setParams({
                "message": component.get("v.sSelectedOption"),
                "channel": "VisualPicklistChannelSetting"
            });
            sendMsgEvent.fire();
        // }
    },

    
})