({
    getLabelForField : function(component, sObjName){
        if(!component.get("v.objLabel")){
            var action = component.get("c.getsFieldLabel");
            action.setParams({
                sObjName : sObjName,
                sFieldAPIName : component.get("v.sFieldName")
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state === "SUCCESS"){
                    var label = response.getReturnValue();
                    component.set("v.sFieldLabel", label);
                } else if(state === "ERROR"){
                    console.log('Error: ' + JSON.stringify(response.error));
                } else {
                    console.log('Unknown problem, state: '+ state + ', error: ' + JSON.stringify(response.error));
                }
                
            });
            $A.enqueueAction(action);
        }
    },
    
    getOptionsForField : function(component, objectName, fieldName, fieldValue) {
        var action = component.get("c.getOptions");
        action.setStorable();
        action.setParams({
            objectAPIName : objectName,
            fieldAPIName: fieldName,
            // fieldValue: fieldValue
        }); 
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var options = response.getReturnValue();
                component.set("v.optionList", options);
            } else if (state === "ERROR"){
                console.log('Error: ' + JSON.stringify(response.error)); 
            } else {
                console.log('Unknown problem, state: ' + state + ', error: ' + JSON.stringify(response.error));
            }
        });
        $A.enqueueAction(action);
    },
    
    evaluateItemSelectedOrNot : function(component,optionValue) {
        if (optionValue) {
            var options=component.find("options");
            for(var j = 0; j < options.length; j++){
                options[j].evaluateSelectedOption(optionValue);
            }
        }
	}
    
    
})