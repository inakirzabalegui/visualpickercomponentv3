({
	doInit : function(component, event, helper) {
 
        helper.evaluateSelected(component, component.get("v.selectedOption") );
	},
	
	//This handler is invoked when the selectedOption is changed.
	handleSelectedOptionChange: function (component, event, helper) {
		helper.evaluateSelected(component, event.getParam("value") );
	},
	
	handleSelection: function (component, event, helper) {
		
		var params = event.getParam('arguments');
		if (params) {
			var optionValue = params.optionValue;
			component.set("v.selectedOption", optionValue);
			//We delegate in handleSelectedOptionChange instead of helper.evaluateSelected(component, event);
		}
	},

	handleClick: function (component, event, helper) {
		var compEvent = component.getEvent("evaluateSelectedOptionEvent");
		var thisOption = component.get("v.thisOption");

		compEvent.setParams({ "optionValue": thisOption.value });
		compEvent.fire();
	},
})