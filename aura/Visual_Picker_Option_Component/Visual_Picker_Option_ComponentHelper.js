({

	evaluateSelected: function (component, selectedOption) {
	
		var thisOption = component.get("v.thisOption");
		var button = component.find("theButton");

		// console.log("event currentOptionValue:"+thisOption.value+" and incoming value:"+selectedOption);

		if (thisOption && (selectedOption === thisOption.value)) {

			$A.util.removeClass(button, 'slds-button_neutral');
			$A.util.addClass(button, 'slds-button_brand');
		}
		else {
			if ($A.util.hasClass(button, 'slds-button_brand')) {
				$A.util.removeClass(button, 'slds-button_brand');
				$A.util.addClass(button, 'slds-button_neutral');
        	 }
		}
	},


})