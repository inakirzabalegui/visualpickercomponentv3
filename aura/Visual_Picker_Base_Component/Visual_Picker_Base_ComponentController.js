({
	doInit: function (component, event, helper) {
		
		// https://success.salesforce.com/ideaView?id=0873A000000CNmCQAW
		var fields = new Array();
		fields.push(component.get("v.fieldToBeSelected"));
		fields.push("Segment_Icon__c");
		component.set("v.fields", fields);
	},

	handleSimpleObjectLoaded: function (component, event, helper){
		//Retrieving support text of the selected option in the base component
		var sObjName = component.get("v.sObjectName");
		var fieldName=component.get("v.fieldToBeSelected");
        if(sObjName){
            helper.getSupportText(component, sObjName,component.get("v.fieldToBeSelected"), component.get("v.simpleObject."+fieldName));
        }
	},

	handleShowModal: function (component, evt, helper) {
		var modalBody;
		var modalFooter;
		var fieldName=component.get("v.fieldToBeSelected");
		$A.createComponents([
			["c:Visual_Picker_Modal_Component",
			 {
				"recordId" : component.get("v.recordId"),//Ilustrative purposes
				"sObjectName" : component.get("v.sObjectName"),	 
				"sFieldName" : fieldName,	
				"sFieldValue" : component.get("v.simpleObject."+fieldName)
			 }],
			 ["c:Visual_Picker_Modal_Footer_Component",{}] 
			],
			function (components, status) {
				if (status === "SUCCESS") {
					modalBody = components[0];
					modalFooter = components[1];
					modalFooter.set("v.body", modalBody );
					component.find('overlayLib').showCustomModal({
						header: "Selecciona la opción",
						body: modalBody,
						footer: modalFooter,
						showCloseButton: true,
						cssClass: "mymodal",
						closeCallback: function () {
							// alert('You closed the alert!');
						}
					})
				}
			});
	},

    handlerMessage : function (component, event, helper){
        var message = event.getParam("message");
        var channel = event.getParam("channel"); //channel = 'VisualPicklistChannel'
        if (channel==="VisualPicklistChannelSetting"){
			var fieldName=component.get("v.fieldToBeSelected");
			component.set("v.simpleObject."+fieldName,message);
			helper.saveObject(component, event, helper);
        }
	},

	handleCancel: function (component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	},
    
    
   
  recordUpdated : function(component, event, helper) {

    var changeType = event.getParams().changeType;

    if (changeType === "ERROR") { /* handle error; do this first! */ }
    else if (changeType === "LOADED") { /* handle record load */ }
    else if (changeType === "REMOVED") { /* handle record removal */ }
    else if (changeType === "CHANGED") { 
      /* handle record change; reloadRecord will cause you to lose your current record, including any changes you’ve made */ 
      component.find("objectRecord").reloadRecord();}
    }

    
    
})