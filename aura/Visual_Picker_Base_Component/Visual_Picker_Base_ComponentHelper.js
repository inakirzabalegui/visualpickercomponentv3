({
	saveObject: function (component, event, helper) {

		var sObjectName  = component.get("v.sObjectName");
		component.find("objectRecord").saveRecord(function (saveResult) {
			if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {

				// Success! Prepare a toast UI message
				var resultsToast = $A.get("e.force:showToast");
				resultsToast.setParams({
					"title": sObjectName+" Saved",
                    "type": 'success',
					"message": "The update was saved."
				});

				// Update the UI: close panel, show toast, refresh object page
				$A.get("e.force:closeQuickAction").fire();
				resultsToast.fire();
                
                component.find("objectRecord").reloadRecord();

				// Reload the view so components not using force:recordData
				// are updated
				$A.get("e.force:refreshView").fire();
			}
			else if (saveResult.state === "INCOMPLETE") {
				console.log("User is offline, device doesn't support drafts.");

				// Error! Prepare a toast UI message
				var resultsToast = $A.get("e.force:showToast");
				resultsToast.setParams({
					"title": sObjectName+" Incomplete",
                    "type": 'error',
					"message": "An error has ocurred."
				});

				// Update the UI: close panel, show toast
				$A.get("e.force:closeQuickAction").fire();
				resultsToast.fire();

			}
			else if (saveResult.state === "ERROR") {
				//console.log('Problem saving '+sObjectName+', error: ' +
				//	JSON.stringify(saveResult.error));

				console.log(saveResult.error[0].pageErrors[0].message);

				// Error! Prepare a toast UI message
				var resultsToast = $A.get("e.force:showToast");
				resultsToast.setParams({
					"title": sObjectName+" error",
                    "type": 'error',
					"message": saveResult.error[0].pageErrors[0].message
				});
				component.find("objectRecord").reloadRecord();
				// Update the UI: close panel, show toast
				$A.get("e.force:closeQuickAction").fire();
				resultsToast.fire();
			}
			else {
				console.log('Unknown problem, state: ' + saveResult.state +
					', error: ' + JSON.stringify(saveResult.error));


				// Error! Prepare a toast UI message
				var resultsToast = $A.get("e.force:showToast");
				resultsToast.setParams({
					"title": sObjectName+"Error",
                    "type": 'error',
					"message": "An unknown error has ocurred."
				});

				// Update the UI: close panel, show toast
				$A.get("e.force:closeQuickAction").fire();
				resultsToast.fire();
			}
		});
	},


	getSupportText : function(component, objectName, fieldName, fieldValue) {

		var action = component.get("c.getSupportText");
		action.setStorable();
        action.setParams({
            objectAPIName : objectName,
            fieldAPIName: fieldName,
            fieldValue: fieldValue
        }); 
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var text = response.getReturnValue();
				component.set("v.supportText", text);
            } else if (state === "ERROR"){
                console.log('Error: ' + JSON.stringify(response.error)); 
            } else {
                console.log('Unknown problem, state: ' + state + ', error: ' + JSON.stringify(response.error));
            }
        });
        $A.enqueueAction(action);
    },
})